package com.ftd.selection.failure.utility.service.service.Impl;

import com.ftd.selection.failure.utility.service.domain.MemberServiceResponse;
import com.ftd.selection.failure.utility.service.service.MemberService;
import com.ftd.selection.failure.utility.service.ws.client.MemberServiceRestApiClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Mono;

@Service
public class MemberServiceImpl implements MemberService {

    @Autowired
    private MemberServiceRestApiClient memberServiceRestApiClient;

    public Mono<MemberServiceResponse> getMemberDetails(String siteId, String memberCode) {
        return memberServiceRestApiClient.getMemberServiceResponse(siteId, memberCode);
    }
}
