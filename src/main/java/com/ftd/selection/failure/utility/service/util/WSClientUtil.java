package com.ftd.selection.failure.utility.service.util;

import com.ftd.selection.failure.utility.service.exception.WSClientException;
import com.ftd.selection.failure.utility.service.exception.WSException;
import io.netty.channel.ConnectTimeoutException;
import io.netty.handler.timeout.ReadTimeoutException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.web.reactive.function.client.WebClientResponseException;
import org.springframework.web.util.UriBuilder;

import java.math.BigDecimal;
import java.net.URI;
import java.time.LocalDate;
import java.util.Objects;

public class WSClientUtil {

    private static final Logger LOG = LoggerFactory.getLogger(WSClientUtil.class);



    public static URI getOrderServiceURI(UriBuilder uriBuilder, String path, String orderId, String deliveryGroupId, String siteId) {
        return uriBuilder.path(path).build(orderId, deliveryGroupId, siteId);
    }

    public static URI getMemberServiceURI(UriBuilder uriBuilder, String path, String siteId, String memberCode) {
        return uriBuilder.path(path).queryParam("isLegacyId", true).build(siteId, memberCode);
    }

    public static URI getDirectoryServiceURI(UriBuilder uriBuilder, String path, String siteId) {
        return uriBuilder.path(path)
                .queryParam("page", 0)
                .queryParam("size", 2000)
                .build(siteId);
    }

    public static URI getAvailabilityServiceURI(UriBuilder uriBuilder, String path, String siteId, String productId,
                                                BigDecimal productPrice, String zipCode, LocalDate deliveryDate,
                                                LocalDate deliveryDateEnd) {
        return uriBuilder.path(path)
                .queryParam("productId", productId)
                .queryParam("zipCode", zipCode)
                .queryParam("startDate", deliveryDate)
                .queryParam("endDate", deliveryDateEnd)
                .queryParam("productPrice", productPrice)
                .build(siteId);
    }

    public static Boolean getWSExceptionPredicate(Throwable throwable) {
        return throwable instanceof ConnectTimeoutException || throwable instanceof ReadTimeoutException
                || (throwable instanceof WebClientResponseException && HttpStatus.INTERNAL_SERVER_ERROR.equals(((WebClientResponseException) throwable).getStatusCode()));
    }

    public static Boolean retryPredicate(Throwable throwable, Integer numRetries) {
        return throwable instanceof WSException && numRetries > 0;
    }

    public static <T> void log(String method, String errorMsg, T data) {
        LOG.error("method_name: {}, error_msg: {}, data: {}", method, errorMsg, data);
    }

    public static <T> Throwable throwWSException(Throwable throwable, String method, T data) {
        log(method, throwable.getMessage(), data);
        return new WSException(throwable.getMessage(), throwable);
    }

    public static Boolean getWSClientExceptionPredicate(Throwable throwable) {
        return !(throwable instanceof WSException);
    }

    public static <T> Throwable throwWSClientException(Throwable throwable, String method, T data) {
        log(method, throwable.getMessage(), data);
        return new WSClientException(throwable.getMessage(), throwable);
    }

    public static String directoryInfoData(String zipCode, String codificationId) {
        StringBuilder data = new StringBuilder();
        data.append("zipCode : ").append(zipCode);
        if (Objects.nonNull(codificationId)) {
            data.append("codificationId : ").append(codificationId);
        }
        return data.toString();
    }

    public static String availabilityInfoData(String productId, BigDecimal productPrice, String zipCode,
                                              LocalDate deliveryDate, LocalDate deliveryDateEnd) {
        return new StringBuilder("productId: ").append(productId)
                .append(" productPrice: ").append(productPrice)
                .append(" zipCode: ").append(zipCode)
                .append(" deliveryDate: ").append(deliveryDate)
                .append(" deliveryDateEnd: ").append(deliveryDateEnd)
                .toString();
    }
}
