package com.ftd.selection.failure.utility.service.ws.client;

import com.ftd.selection.failure.utility.service.domain.AvailabilityServiceResponse;
import com.ftd.selection.failure.utility.service.util.WSClientUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;
import reactor.core.publisher.SignalType;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;
import java.util.logging.Level;

@Component
public class AvailabilityServiceRestApiClient {

    private static final Logger LOG = LoggerFactory.getLogger(AvailabilityServiceRestApiClient.class);

    private static final String URI_AVAILABILITY_SERVICE = "{siteId}/api/floristavailabilities";

    @Autowired
    @Qualifier(value = "availabilityServiceWebClient")
    private WebClient webClient;

    @Value("${properties.directory.service.retryable-properties.numRetries}")
    private Integer numRetries;

    public Mono<List<AvailabilityServiceResponse>> getAvailabilityServiceResponse(String siteId, String productId, BigDecimal productPrice,
                                                                                  String zipCode, LocalDate deliveryDate, LocalDate deliveryDateEnd) {
        return webClient.get()
                .uri(uriBuilder -> WSClientUtil.getAvailabilityServiceURI(uriBuilder, URI_AVAILABILITY_SERVICE, siteId,
                        productId, productPrice, zipCode, deliveryDate, deliveryDateEnd))
                .retrieve()
                .bodyToMono(new ParameterizedTypeReference<List<AvailabilityServiceResponse>>() {})
                .onErrorMap(WSClientUtil::getWSExceptionPredicate,
                        throwable -> WSClientUtil.throwWSException(throwable, "getAvailableFlorists",
                                WSClientUtil.availabilityInfoData(productId, productPrice, zipCode, deliveryDate, deliveryDateEnd)))
                .retry(numRetries,
                        throwable -> WSClientUtil.retryPredicate(throwable, numRetries))
                .onErrorMap(WSClientUtil::getWSClientExceptionPredicate,
                        throwable -> WSClientUtil.throwWSClientException(throwable, "getAvailableFlorists", null))
                .log("availability-service.reactor.",
                        Level.INFO,
                        SignalType.CANCEL,
                        SignalType.ON_ERROR,
                        SignalType.ON_COMPLETE);
    }
}
