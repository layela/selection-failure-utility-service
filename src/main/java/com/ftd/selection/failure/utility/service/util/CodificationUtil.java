package com.ftd.selection.failure.utility.service.util;

import com.ftd.selection.failure.utility.service.domain.Codification;
import org.springframework.core.io.ClassPathResource;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.Optional;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public class CodificationUtil {

    public static List<Codification> readCodifications() throws IOException {
        ClassPathResource classPathResource = new ClassPathResource("codificationMaster.csv");
        BufferedReader br = new BufferedReader(
                new InputStreamReader(classPathResource.getInputStream(), StandardCharsets.UTF_8));
        Pattern csvPattern = Pattern.compile(",");
        return br.lines().map(line -> {
            String[] record = csvPattern.split(line);
            Codification codification = Codification.builder().code(record[Integer.valueOf("0")])
                    .category(record[Integer.valueOf("1")]).description(record[Integer.valueOf("2")])
                    .codificationAllFloristFlag(record[Integer.valueOf("3")])
                    .codificationRequired(record[Integer.valueOf("4")]).build();
            return codification;
        }).collect(Collectors.toList());
    }


    public static Codification getCodificationInfo(String code) {
        List<Codification> codificationList = null;
        try {
            codificationList = readCodifications();
            Optional<Codification> codification = codificationList.stream().filter(c -> c.getCode().equalsIgnoreCase(code)).findFirst();
            if (codification.isPresent()) {
                return codification.get();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return new Codification();
    }
}
