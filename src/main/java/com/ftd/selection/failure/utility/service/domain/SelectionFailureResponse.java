package com.ftd.selection.failure.utility.service.domain;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.ftd.selection.failure.utility.service.constants.ReasonCode;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@JsonPropertyOrder({"siteId", "orderId", "selectionFailureDetails" })
public class SelectionFailureResponse implements Serializable{
    private static final long serialVersionUID = 1L;

    private String siteId;
    private String orderId;
    private List<SelectionFailureInfo> selectionFailureDetails;

    @Data
    @Builder
    @AllArgsConstructor
    @NoArgsConstructor
    @JsonPropertyOrder({"deliveryGroupId", "siteId", "productId", "codificationId", "codificationCategory",
            "zipCode", "productPrice", "deliveryDate", "deliveryDateEnd", "excludedFlorists", "availableFlorists",
            "floristDetails"})
    public static class SelectionFailureInfo implements Serializable {
        private static final long serialVersionUID = 1L;

        // Order attributes
        private String deliveryGroupId;
        private String siteId;
        private String productId;
        private String codificationId;
        private String codificationCategory;
        private String zipCode;
        private BigDecimal productPrice;
        @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
        private LocalDate deliveryDate;
        @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
        private LocalDate deliveryDateEnd;
        private List<String> excludedFlorists;
        // Available Florists
        private List<AvailableFloristsInfo> availableFlorists;

        // Florist Details
        private List<FloristInfo> floristDetails;
    }

    @Data
    @Builder
    @AllArgsConstructor
    @NoArgsConstructor
    @JsonPropertyOrder({"deliveryDate", "argoFlorists", "manualFlorists" })
    public static class AvailableFloristsInfo implements Serializable {
        private static final long serialVersionUID = 1L;
        @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
        private LocalDate deliveryDate;
        private List<String> argoFlorists;
        private List<String> manualFlorists;
    }

    @Data
    @Builder
    @AllArgsConstructor
    @NoArgsConstructor
    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonPropertyOrder({"memberCode", "productMinimum", "floristMinimum", "status", "details" })
    public static class FloristInfo implements Serializable {
        private static final long serialVersionUID = 1L;

        private String memberCode;
        private String productMinimum;
        private String floristMinimum;
        private String status;
        private List<Details> details;

    }

    @Data
    @Builder
    @AllArgsConstructor
    @NoArgsConstructor
    @JsonPropertyOrder({"deliveryDate", "metaData"})
    public static class Details implements Serializable {
        private static final long serialVersionUID = 1L;
        @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
        private LocalDate deliveryDate;
        private List<MetaData> metaData;
    }

    @Data
    @Builder
    @AllArgsConstructor
    @NoArgsConstructor
    @JsonPropertyOrder({"reasonCode", "reasonData"})
    public static class MetaData implements Serializable {
        private static final long serialVersionUID = 1L;
        private ReasonCode reasonCode;
        private ReasonData reasonData;
    }

    @Data
    @Builder
    @AllArgsConstructor
    @NoArgsConstructor
    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonPropertyOrder({"suspendStartDate", "suspendEndDate", "startDate", "endDate", "type",
            "isShopOpen", "floristMinimum", "productMinimum"})
    public static class ReasonData implements Serializable {
        private static final long serialVersionUID = 1L;

        // Below two fields are for suspend blocks bcz it has hours.
        @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'")
        private LocalDateTime suspendStartDate;
        @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'")
        private LocalDateTime suspendEndDate;
        // Below two fields are for zip blocks, codification blocks, florist blocks,
        @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
        private LocalDate startDate;
        @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
        private LocalDate endDate;
        //This is for suspend type or florist block type
        private String type;

        //This is to set shop open or not
        private Boolean isShopOpen;

        private String cutoffTime;
        private String memberTimeZone;

        private BigDecimal floristMinimum;
        private BigDecimal productMinimum;
    }
}
