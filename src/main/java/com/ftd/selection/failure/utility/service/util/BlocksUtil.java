package com.ftd.selection.failure.utility.service.util;

import com.ftd.selection.failure.utility.service.config.PmClosedConfig;
import com.ftd.selection.failure.utility.service.constants.CommonConstants;
import com.ftd.selection.failure.utility.service.constants.DeliveryOption;
import com.ftd.selection.failure.utility.service.constants.DeliveryZone;
import com.ftd.selection.failure.utility.service.domain.AvailabilityExceptionInfo;
import com.ftd.selection.failure.utility.service.domain.DeliveryZoneInfo;
import com.ftd.selection.failure.utility.service.domain.MemberServiceResponse;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.StringUtils;

import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public class BlocksUtil {

    public static final Integer TWELVE = 12;

    public static Boolean isShopOpen(MemberServiceResponse.OperationalHoursInfo operationalHoursInfo,
                                     LocalDateTime memberDateTime,
                                     LocalDate deliveryDate, PmClosedConfig pmClosedConfig) {
        MemberServiceResponse.OperationalHoursInfo.OperationalHour operationalHour = null;
        MemberServiceResponse.OperationalHoursInfo.WorkingHours sameDayHours = null;
        MemberServiceResponse.OperationalHoursInfo.WorkingHours deliveryDayHours = null;
        List<MemberServiceResponse.OperationalHoursInfo.WorkingHourOverride> overrides = null;
        if (Objects.nonNull(operationalHoursInfo) &&
                CollectionUtils.isNotEmpty(operationalHoursInfo.getOperationalHours())) {
            operationalHour = operationalHoursInfo.getOperationalHours().stream()
                    .filter(ele -> "WEBSITE".equalsIgnoreCase(ele.getChannel()))
                    .findFirst().get();
        }
        if (Objects.nonNull(operationalHour)) {
            sameDayHours =
                    getWorkingHoursForADay(operationalHour.getOpenHours(), memberDateTime.getDayOfWeek().name());
            deliveryDayHours =
                    getWorkingHoursForADay(operationalHour.getOpenHours(), deliveryDate.getDayOfWeek().name());
            overrides = operationalHour.getOpenHoursOverrides();
        }
        if (!isShopOpened(sameDayHours) && hasOverride(overrides, memberDateTime.toLocalDate())) {
            sameDayHours.setCode("");
            sameDayHours.setOpenedFlag("T");
        }
        if (!isShopOpened(sameDayHours) && hasOverride(overrides, deliveryDate)) {
            deliveryDayHours.setCode("");
            deliveryDayHours.setOpenedFlag("T");
        }

        return isShopOpened(sameDayHours, deliveryDayHours, memberDateTime, deliveryDate, pmClosedConfig);
    }

    private static boolean isShopOpened(MemberServiceResponse.OperationalHoursInfo.WorkingHours sameDayHours,
                                        MemberServiceResponse.OperationalHoursInfo.WorkingHours deliveryDayHours,
                                        LocalDateTime memberDateTime, LocalDate deliveryDate,
                                        PmClosedConfig pmClosedConfig) {
        Boolean isShopOpened = Boolean.FALSE;
        if (isFullDayClosed(sameDayHours)) {
            if (isSunday(memberDateTime.toLocalDate()) && isAmOpen(deliveryDayHours)) {
                isShopOpened = Boolean.TRUE;
            }
        } else if (isFullDayOpen(sameDayHours)) {
            isShopOpened = isAmOpen(deliveryDayHours);
        } else if (isAmClosure(memberDateTime) && isAmOpen(sameDayHours)) {
            isShopOpened = (isSameDayDelivery(deliveryDate) && isBeforeTime(memberDateTime,
                    pmClosedConfig.getPmClosedDeliveryCutOff())) ||
                    (!isSameDayDelivery(deliveryDate) && isBeforeTime(memberDateTime,
                            pmClosedConfig.getPmClosedOrderCutOff()));
        } else if (isPmClosure(memberDateTime)) {
            isShopOpened = !isSameDayDelivery(deliveryDate) && isAmOpen(deliveryDayHours);
        }
        return isShopOpened;
    }

    public static boolean isBeforeTime(LocalDateTime memberDateTime, String pmClosedDeliveryCutoff) {
        return memberDateTime.isBefore(getLocalDateTime(memberDateTime.toLocalDate(), pmClosedDeliveryCutoff));
    }

    public static LocalDateTime getLocalDateTime(LocalDate localDate, String cutoff) {
        int hour = Integer.parseInt(cutoff.substring(0, 2));
        int minutes = Integer.parseInt(cutoff.substring(2));
        return LocalDateTime.of(localDate, LocalTime.of(hour, minutes));
    }

    private static boolean isSunday(LocalDate localDate) {
        return localDate.getDayOfWeek().equals(DayOfWeek.SUNDAY);
    }

    private static boolean hasOverride(List<MemberServiceResponse.OperationalHoursInfo.WorkingHourOverride> overrides
            , LocalDate date) {
        return CollectionUtils.isNotEmpty(overrides) && overrides.stream().anyMatch(override -> checkForOverride(date
                , override));
    }

    private static boolean isAmOpen(MemberServiceResponse.OperationalHoursInfo.WorkingHours workingHoursForADay) {
        return isFullDayOpen(workingHoursForADay)
                || (Objects.nonNull(workingHoursForADay) && !CommonConstants.AM_CLOSED.equalsIgnoreCase(workingHoursForADay.getCode())
                && !isFullDayClosed(workingHoursForADay));
    }

    private static boolean isAmClosure(LocalDateTime memberDateTime) {
        return memberDateTime.getHour() < TWELVE;
    }

    private static boolean isPmClosure(LocalDateTime memberDateTime) {
        return memberDateTime.getHour() >= TWELVE;
    }

    public static boolean isSameDayDelivery(LocalDate localDate) {
        return LocalDate.now().isEqual(localDate);
    }

    private static boolean isFullDayOpen(MemberServiceResponse.OperationalHoursInfo.WorkingHours workingHoursForADay) {
        return Objects.nonNull(workingHoursForADay) && StringUtils.isEmpty(workingHoursForADay.getCode());
    }

    private static boolean isFullDayClosed(MemberServiceResponse.OperationalHoursInfo.WorkingHours workingHoursForADay) {
        return Objects.isNull(workingHoursForADay) || CommonConstants.FULL_DAY_CLOSED.equalsIgnoreCase(workingHoursForADay.getCode());
    }

    private static boolean checkForOverride(LocalDate deliveryDate,
                                            MemberServiceResponse.OperationalHoursInfo.WorkingHourOverride override) {
        return deliveryDate.compareTo(override.getStartDate()) >= 0 &&
                deliveryDate.compareTo(override.getEndDate()) <= 0;
    }

    private static boolean isShopOpened(MemberServiceResponse.OperationalHoursInfo.WorkingHours workingHours) {
        return Objects.nonNull(workingHours) && BooleanUtils.toBoolean(workingHours.getOpenedFlag());
    }

    private static MemberServiceResponse.OperationalHoursInfo.WorkingHours getWorkingHoursForADay(List<MemberServiceResponse.OperationalHoursInfo.WorkingHours> workingHours,
                                                                                                  String dayOfWeek) {
        if (CollectionUtils.isNotEmpty(workingHours)) {
            return workingHours.stream().filter(d -> dayOfWeek.equalsIgnoreCase(d.getDay()))
                    .findAny().orElse(null);
        }
        return null;
    }

    public static Boolean isNotBeforeCutoffTime(String cutoffTime, LocalDate deliveryDate,
                                                LocalDateTime memberDateTime) {
        Boolean isBeforeCutoffTime = Boolean.TRUE;
        if (Objects.nonNull(cutoffTime)) {
            Integer hour = Integer.parseInt(cutoffTime.substring(0, 2));
            Integer minutes = Integer.parseInt(cutoffTime.substring(3, 5));
            LocalDateTime localDateTime = LocalDateTime.of(deliveryDate, LocalTime.of(hour, minutes));
            isBeforeCutoffTime = memberDateTime.isBefore(localDateTime);
        }
        return isBeforeCutoffTime;
    }

    public static String getCutoffTime(DeliveryZoneInfo deliveryZoneInfo, String zipCode, LocalDate deliveryDate,
                                       LocalDateTime memberDateTime) {
        if (Objects.nonNull(deliveryZoneInfo) && CollectionUtils.isNotEmpty(deliveryZoneInfo.getDeliveryZones())) {
            DeliveryZoneInfo.CutoffTime cutoffTime = null;
            Optional<DeliveryZoneInfo.DeliveryZoneGen> deliveryZoneGen = deliveryZoneInfo.getDeliveryZones()
                    .parallelStream()
                    .filter(ele -> "WEBSITE".equalsIgnoreCase(ele.getChannel()) && DeliveryZone.POSTAL_CODE.equals(ele.getType()))
                    .collect(Collectors.toList())
                    .parallelStream()
                    .filter(deliveryZone -> deliveryZone.getZoneTypeInfo().getZipCodeInfo().getZipcode().equalsIgnoreCase(zipCode))
                    .findAny();
            if (deliveryZoneGen.isPresent()) {
                cutoffTime = deliveryZoneGen.get()
                        .getProductGroups().stream().filter(productGroup -> productGroup.getValue().contains(
                                "SUBSCRIBED_CODIFIED_PRODUCTS"))
                        .findFirst().get()
                        .getDeliveryOptions().stream().filter(deliveryOption -> deliveryOption.getDeliveryOption().equals(DeliveryOption.STANDARD_DELIVERY))
                        .findFirst().get()
                        .getCutoffTime();
            }
            return getCutoffTime(cutoffTime.getLocal(), deliveryDate);
        }
        return null;
    }

    public static String getCutoffTime(DeliveryZoneInfo.WeekDayCutoff localCutoffTime, LocalDate deliveryDate) {
        String cutoffTime = null;
        if (deliveryDate.getDayOfWeek().equals(DayOfWeek.MONDAY)) {
            cutoffTime = localCutoffTime.getMon();
        } else if (deliveryDate.getDayOfWeek().equals(DayOfWeek.TUESDAY)) {
            cutoffTime = localCutoffTime.getTue();
        } else if (deliveryDate.getDayOfWeek().equals(DayOfWeek.WEDNESDAY)) {
            cutoffTime = localCutoffTime.getWed();
        } else if (deliveryDate.getDayOfWeek().equals(DayOfWeek.THURSDAY)) {
            cutoffTime = localCutoffTime.getThu();
        } else if (deliveryDate.getDayOfWeek().equals(DayOfWeek.FRIDAY)) {
            cutoffTime = localCutoffTime.getFri();
        } else if (deliveryDate.getDayOfWeek().equals(DayOfWeek.SATURDAY)) {
            cutoffTime = localCutoffTime.getSat();
        } else if (deliveryDate.getDayOfWeek().equals(DayOfWeek.SUNDAY)) {
            cutoffTime = localCutoffTime.getSun();
        }
        return cutoffTime;
    }

    public static AvailabilityExceptionInfo.ZipInfo getZipBlockInfo(AvailabilityExceptionInfo availabilityExceptionInfo,
                                                                    String zipCode, LocalDate deliveryDate) {
        return Optional.ofNullable(availabilityExceptionInfo)
                .map(AvailabilityExceptionInfo::getZipBlocks)
                .orElse(new ArrayList<>())
                .stream()
                .filter(getZipBlockPredicate(zipCode, deliveryDate))
                .findFirst().orElse(null);
    }

    public static AvailabilityExceptionInfo.CodificationInfo getCodificationBlockInfo(AvailabilityExceptionInfo availabilityExceptionInfo,
                                                                                      String productGroup,
                                                                                      LocalDate deliveryDate) {

        return Optional.ofNullable(availabilityExceptionInfo)
                .map(AvailabilityExceptionInfo::getCodificationBlocks)
                .orElse(new ArrayList<>())
                .stream()
                .filter(blocksInfo
                        -> blocksInfo.getProductGroup().equalsIgnoreCase(productGroup)
                        && checkForBlockDate(deliveryDate, blocksInfo))
                .findFirst()
                .orElse(null);
    }

    public static AvailabilityExceptionInfo.FloristBlockInfo getFloristBlockInfo(AvailabilityExceptionInfo availabilityExceptionInfo,
                                                                                 LocalDate deliveryDate) {
        return Optional.ofNullable(availabilityExceptionInfo)
                .map(AvailabilityExceptionInfo::getFloristBlocks)
                .orElse(new ArrayList<>())
                .stream()
                .filter(blocksInfo -> checkForBlockDate(deliveryDate, blocksInfo))
                .findFirst()
                .orElse(null);
    }

    public static AvailabilityExceptionInfo.SuspendInfo getSuspendBlockInfo(AvailabilityExceptionInfo availabilityExceptionInfo,
                                                                            LocalDate deliveryDate) {
        return Optional.ofNullable(availabilityExceptionInfo)
                .map(AvailabilityExceptionInfo::getSuspendBlocks)
                .orElse(new ArrayList<>())
                .stream().filter(blocksInfo
                        -> checkForSuspendsBlockDate(deliveryDate, blocksInfo))
                .findFirst()
                .orElse(null);
    }

    private static Predicate<AvailabilityExceptionInfo.ZipInfo> getZipBlockPredicate(String zipCode,
                                                                                     LocalDate deliveryDate) {
        return blocksInfo ->
                zipCode.equalsIgnoreCase(blocksInfo.getZipCode())
                        && checkForBlockDate(deliveryDate, blocksInfo);
    }

    public static LocalDate getLocalDate(LocalDateTime aLocalDateTime) {
        if (Objects.nonNull(aLocalDateTime)) {
            return aLocalDateTime.toLocalDate();
        }
        return null;
    }

    private static boolean checkForBlockDate(LocalDate deliveryDate, AvailabilityExceptionInfo.DateRange dateRange) {
        if (Objects.isNull(dateRange.getEndDate())) {
            return deliveryDate.compareTo(dateRange.getStartDate().toLocalDate()) >= 0;
        }
        return deliveryDate.compareTo(dateRange.getStartDate().toLocalDate()) >= 0 &&
                deliveryDate.compareTo(dateRange.getEndDate().toLocalDate()) <= 0;
    }

    private static boolean checkForSuspendsBlockDate(LocalDate deliveryDate,
                                                     AvailabilityExceptionInfo.DateRange dateRange) {
        LocalDate orderDate = LocalDate.now();
        Boolean isSuspended = false;
        if (orderDate.compareTo(dateRange.getStartDate().toLocalDate()) >= 0 &&
                (Objects.isNull(dateRange.getEndDate()) || orderDate.compareTo(dateRange.getEndDate().toLocalDate()) <= 0)) {
            //Current Suspend
            isSuspended = true;
        } else if (orderDate.compareTo(dateRange.getStartDate().toLocalDate()) < 0 &&
                (Objects.isNull(dateRange.getEndDate()) || orderDate.compareTo(dateRange.getEndDate().toLocalDate()) < 0)) {
            //Future Suspend
            isSuspended = false;
        }
        return isSuspended;
    }
}
