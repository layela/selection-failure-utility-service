package com.ftd.selection.failure.utility.service.domain;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Codification implements Serializable {
    private static final long serialVersionUID = 1L;

    private String code;
    private String category;
    private String description;
    private String codificationAllFloristFlag;
    private String codificationRequired;

}
