package com.ftd.selection.failure.utility.service.domain;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.List;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class DirectoryServiceResponse implements Serializable {

    private static final long serialVersionUID = 1L;
    private List<MemberMetaData> content;

    @Data
    @Builder
    @AllArgsConstructor
    @NoArgsConstructor
    public static class MemberMetaData implements Serializable {
        private static final long serialVersionUID = 1L;
        private String id;
    }
}
