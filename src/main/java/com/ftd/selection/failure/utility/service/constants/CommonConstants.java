package com.ftd.selection.failure.utility.service.constants;

public final class CommonConstants {

    public static final String AM_CLOSED = "A";
    public static final String FULL_DAY_CLOSED = "F";
    public static final String OTHER = "Other";
}
