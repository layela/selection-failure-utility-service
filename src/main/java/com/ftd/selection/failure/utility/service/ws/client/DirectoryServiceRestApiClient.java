package com.ftd.selection.failure.utility.service.ws.client;

import com.ftd.selection.failure.utility.service.domain.DirectoryServiceRequest;
import com.ftd.selection.failure.utility.service.domain.DirectoryServiceResponse;
import com.ftd.selection.failure.utility.service.util.WSClientUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.BodyInserters;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;
import reactor.core.publisher.SignalType;

import java.util.logging.Level;

@Component
public class DirectoryServiceRestApiClient {

    private static final Logger LOG = LoggerFactory.getLogger(DirectoryServiceRestApiClient.class);

    private static final String URI_DIRECTORY_SERVICE = "{siteId}/members/search/page";

    @Autowired
    @Qualifier(value = "directoryServiceWebClient")
    private WebClient webClient;

    @Value("${properties.directory.service.retryable-properties.numRetries}")
    private Integer numRetries;

    public Mono<DirectoryServiceResponse> getDirectoryServiceResponse(String siteId, DirectoryServiceRequest directoryServiceRequest) {
        return webClient.post()
                .uri(uriBuilder -> WSClientUtil.getDirectoryServiceURI(uriBuilder, URI_DIRECTORY_SERVICE, siteId))
                .body(BodyInserters.fromObject(directoryServiceRequest))
                .retrieve()
                .bodyToMono(DirectoryServiceResponse.class)
                .onErrorMap(WSClientUtil::getWSExceptionPredicate,
                        throwable -> WSClientUtil.throwWSException(throwable, "getMembers",
                                WSClientUtil.directoryInfoData(directoryServiceRequest.getDeliveryZipcode(), directoryServiceRequest.getWebsiteProductGroup())))
                .retry(numRetries,
                        throwable -> WSClientUtil.retryPredicate(throwable, numRetries))
                .onErrorMap(WSClientUtil::getWSClientExceptionPredicate,
                        throwable -> WSClientUtil.throwWSClientException(throwable, "getMembers", null))
                .log("order-service.reactor.",
                        Level.INFO,
                        SignalType.CANCEL,
                        SignalType.ON_ERROR,
                        SignalType.ON_COMPLETE);
    }
}
