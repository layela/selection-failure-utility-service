package com.ftd.selection.failure.utility.service.constants;

public enum Status {
    ACTIVE, BLOCKED
}
