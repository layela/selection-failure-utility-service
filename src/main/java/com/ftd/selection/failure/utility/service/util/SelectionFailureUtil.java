package com.ftd.selection.failure.utility.service.util;

import com.ftd.selection.failure.utility.service.config.PmClosedConfig;
import com.ftd.selection.failure.utility.service.constants.ReasonCode;
import com.ftd.selection.failure.utility.service.domain.AvailabilityExceptionInfo;
import com.ftd.selection.failure.utility.service.domain.AvailabilityServiceResponse;
import com.ftd.selection.failure.utility.service.domain.DeliveryZoneInfo;
import com.ftd.selection.failure.utility.service.domain.MemberServiceResponse;
import com.ftd.selection.failure.utility.service.domain.OrderSearchResponse;
import com.ftd.selection.failure.utility.service.domain.SelectionFailureResponse;
import org.apache.commons.collections4.CollectionUtils;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

public class SelectionFailureUtil {

    public static SelectionFailureResponse.SelectionFailureInfo setOrderDetails(OrderSearchResponse.DeliveryGroup deliveryGroup) {

        SelectionFailureResponse.SelectionFailureInfo.SelectionFailureInfoBuilder builder = SelectionFailureResponse.SelectionFailureInfo.builder();
        builder.deliveryGroupId(deliveryGroup.getDeliveryGroupId());
        builder.deliveryDate(deliveryGroup.getDeliveryDate());
        if (Objects.isNull(deliveryGroup.getDeliveryDateEnd())) {
            builder.deliveryDateEnd(deliveryGroup.getDeliveryDate());
        } else {
            builder.deliveryDateEnd(deliveryGroup.getDeliveryDateEnd());
        }
        if (CollectionUtils.isNotEmpty(deliveryGroup.getLineItems())) {
            OrderSearchResponse.LineItem lineItem = deliveryGroup.getLineItems().get(0);
            builder.productId(lineItem.getProductId());
            builder.codificationId(lineItem.getCodificationId());
            builder.codificationCategory(CodificationUtil.getCodificationInfo(lineItem.getCodificationId()).getCategory());
            if (Objects.nonNull(lineItem.getLineItemAmounts())) {
                builder.productPrice(new BigDecimal(lineItem.getLineItemAmounts().getRetailProductAmount()));
            }
        }
        if (Objects.nonNull(deliveryGroup.getRecipientDetails())) {
            builder.zipCode(deliveryGroup.getRecipientDetails().getPostalCode());
        }
        builder.excludedFlorists(deliveryGroup.getRejectedFulfillingMembers());
        return builder.build();
    }

    public static Mono<SelectionFailureResponse.SelectionFailureInfo> analyzeBlocks(List<MemberServiceResponse> members,
                                                                                    SelectionFailureResponse.SelectionFailureInfo selectionFailureInfo,
                                                                                    PmClosedConfig pmClosedConfig) {
        return Flux.fromIterable(members).parallel()
                .flatMap(ele -> setFloristInfo(ele, selectionFailureInfo, pmClosedConfig))
                .sequential()
                .collectList()
                .flatMap(ele -> {
                    selectionFailureInfo.setFloristDetails(ele);
                    return Mono.just(selectionFailureInfo);
                });
    }

    public static Mono<SelectionFailureResponse.FloristInfo> setFloristInfo(MemberServiceResponse memberServiceResponse,
                                                                            SelectionFailureResponse.SelectionFailureInfo selectionFailureInfo,
                                                                            PmClosedConfig pmClosedConfig) {
        SelectionFailureResponse.FloristInfo.FloristInfoBuilder builder = SelectionFailureResponse.FloristInfo.builder();
        builder.memberCode(memberServiceResponse.getIdentity().getMemberCode());
        return getDeliveryDates(selectionFailureInfo)
                .map(deliveryDate -> setDetails(deliveryDate, memberServiceResponse.getMember(), selectionFailureInfo, pmClosedConfig))
                .collectList()
                .map(ele -> {
                    builder.details(ele);
                    return builder.build();
                });
    }

    /*
     * Set meta data for each delivery date.
     * */
    public static SelectionFailureResponse.Details setDetails(LocalDate deliveryDate, MemberServiceResponse.Member member,
                                                              SelectionFailureResponse.SelectionFailureInfo selectionFailureInfo,
                                                              PmClosedConfig pmClosedConfig) {
        return SelectionFailureResponse.Details.builder()
                .deliveryDate(deliveryDate)
                .metaData(getMetaData(deliveryDate, member, selectionFailureInfo, pmClosedConfig))
                .build();
    }

    public static List<SelectionFailureResponse.MetaData> getMetaData(LocalDate deliveryDate, MemberServiceResponse.Member member,
                                                                      SelectionFailureResponse.SelectionFailureInfo selectionFailureInfo,
                                                                      PmClosedConfig pmClosedConfig) {
        List<SelectionFailureResponse.MetaData> metaData = new ArrayList<>();
        String memberTimeZone = getTimeZone(member.getMemberInfo().getGeoInfo());
        LocalDateTime memberDateTime = getMemberLocalDateTime(member.getMemberInfo().getGeoInfo());
        setCutoffTimeData(member.getDeliveryZoneInfo(), selectionFailureInfo.getZipCode(), deliveryDate, memberDateTime, memberTimeZone, metaData);
        setShopOpenData(member.getOperationalHoursInfo(), memberDateTime, deliveryDate, metaData, pmClosedConfig);
        setZipBlocksData(member.getAvailabilityExceptionInfo(), selectionFailureInfo.getZipCode(), deliveryDate, metaData);
        setCodificationBlocksData(member.getAvailabilityExceptionInfo(), selectionFailureInfo.getCodificationId(), deliveryDate, metaData);
        setFloristBlocksData(member.getAvailabilityExceptionInfo(), deliveryDate, metaData);
        setSuspendBlocksData(member.getAvailabilityExceptionInfo(), deliveryDate, metaData);
        setFloristMinimumData(member.getMemberInfo(), selectionFailureInfo, metaData);
        // removed as per argo logic. The logic to handle this will change
        //setProductMinimumData(member.getCodificationInfo(), selectionFailureInfo, metaData);

        // This is to indicate that florist is available :: FS_AV
        if (CollectionUtils.isEmpty(metaData)) {
            metaData.add(SelectionFailureResponse.MetaData.builder().reasonCode(ReasonCode.FS_AV).build());
        }
        return metaData;
    }

    public static LocalDateTime getMemberLocalDateTime(MemberServiceResponse.GeoInfo geoInfo) {
        LocalDateTime memberTimeZone = LocalDateTime.now();
        if (Objects.nonNull(geoInfo) && Objects.nonNull(geoInfo.getTzData())) {
            memberTimeZone = DateUtil.convertUtcToMemberTimeZone(geoInfo.getTzData());
        }
        return memberTimeZone;
    }

    public static String getTimeZone(MemberServiceResponse.GeoInfo geoInfo) {
        String memberTimeZone = "UTC";
        if (Objects.nonNull(geoInfo) && Objects.nonNull(geoInfo.getTzData())) {
            memberTimeZone = geoInfo.getTimeZone();
        }
        return memberTimeZone;
    }

    public static void setCutoffTimeData(DeliveryZoneInfo deliveryZoneInfo, String zipCode, LocalDate deliveryDate, LocalDateTime memberDateTime,
                                         String memberTimeZone, List<SelectionFailureResponse.MetaData> metaData) {
        String cutoffTime = BlocksUtil.getCutoffTime(deliveryZoneInfo, zipCode, deliveryDate, memberDateTime);
        if (BlocksUtil.isNotBeforeCutoffTime(cutoffTime, deliveryDate, memberDateTime)
                && BlocksUtil.isSameDayDelivery(deliveryDate)) {
            metaData.add(SelectionFailureResponse.MetaData.builder()
                    .reasonCode(ReasonCode.FS_CO)
                    .reasonData(SelectionFailureResponse.ReasonData.builder()
                            .cutoffTime(cutoffTime)
                            .memberTimeZone(memberTimeZone)
                            .build())
                    .build());
        }
    }

    public static void setCodificationBlocksData(AvailabilityExceptionInfo availabilityExceptionInfo,
                                                 String codificationId, LocalDate deliveryDate, List<SelectionFailureResponse.MetaData> metaData) {
        AvailabilityExceptionInfo.CodificationInfo codificationBlockInfo = BlocksUtil.getCodificationBlockInfo(availabilityExceptionInfo, codificationId, deliveryDate);
        if (Objects.nonNull(codificationBlockInfo)) {
            metaData.add(SelectionFailureResponse.MetaData.builder()
                    .reasonCode(ReasonCode.FS_CIB)
                    .reasonData(SelectionFailureResponse.ReasonData.builder()
                            .startDate(BlocksUtil.getLocalDate(codificationBlockInfo.getStartDate()))
                            .endDate(BlocksUtil.getLocalDate(codificationBlockInfo.getEndDate()))
                            .build())
                    .build());
        }
    }

    public static void setZipBlocksData(AvailabilityExceptionInfo availabilityExceptionInfo,
                                        String zipCode, LocalDate deliveryDate, List<SelectionFailureResponse.MetaData> metaData) {
        AvailabilityExceptionInfo.ZipInfo zipInfo = BlocksUtil.getZipBlockInfo(availabilityExceptionInfo, zipCode, deliveryDate);
        if (Objects.nonNull(zipInfo)) {
            metaData.add(SelectionFailureResponse.MetaData.builder()
                    .reasonCode(ReasonCode.FS_ZB)
                    .reasonData(SelectionFailureResponse.ReasonData.builder()
                            .startDate(BlocksUtil.getLocalDate(zipInfo.getStartDate()))
                            .endDate(BlocksUtil.getLocalDate(zipInfo.getEndDate()))
                            .build())
                    .build());
        }
    }

    public static void setFloristBlocksData(AvailabilityExceptionInfo availabilityExceptionInfo,
                                            LocalDate deliveryDate, List<SelectionFailureResponse.MetaData> metaData) {
        AvailabilityExceptionInfo.FloristBlockInfo floristBlockInfo = BlocksUtil.getFloristBlockInfo(availabilityExceptionInfo, deliveryDate);
        if (Objects.nonNull(floristBlockInfo)) {
            metaData.add(SelectionFailureResponse.MetaData.builder()
                    .reasonCode(ReasonCode.FS_FB)
                    .reasonData(SelectionFailureResponse.ReasonData.builder()
                            .startDate(BlocksUtil.getLocalDate(floristBlockInfo.getStartDate()))
                            .endDate(BlocksUtil.getLocalDate(floristBlockInfo.getEndDate()))
                            .type(floristBlockInfo.getType())
                            .build())
                    .build());
        }
    }

    public static void setSuspendBlocksData(AvailabilityExceptionInfo availabilityExceptionInfo,
                                            LocalDate deliveryDate, List<SelectionFailureResponse.MetaData> metaData) {
        AvailabilityExceptionInfo.SuspendInfo suspendBlockInfo = BlocksUtil.getSuspendBlockInfo(availabilityExceptionInfo, deliveryDate);
        if (Objects.nonNull(suspendBlockInfo)) {
            metaData.add(SelectionFailureResponse.MetaData.builder()
                    .reasonCode(ReasonCode.FS_SB)
                    .reasonData(SelectionFailureResponse.ReasonData.builder()
                            .suspendStartDate(suspendBlockInfo.getStartDate())
                            .suspendEndDate(suspendBlockInfo.getEndDate())
                            .type(suspendBlockInfo.getType())
                            .build())
                    .build());
        }
    }

    public static void setShopOpenData(MemberServiceResponse.OperationalHoursInfo operationalHoursInfo, LocalDateTime memberDateTime,
                                       LocalDate deliveryDate, List<SelectionFailureResponse.MetaData> metaData,
                                       PmClosedConfig pmClosedConfig) {
        if (!BlocksUtil.isShopOpen(operationalHoursInfo, memberDateTime, deliveryDate, pmClosedConfig)) {
            metaData.add(SelectionFailureResponse.MetaData.builder()
                    .reasonCode(ReasonCode.FS_SC)
                    .reasonData(SelectionFailureResponse.ReasonData.builder().isShopOpen(Boolean.FALSE).build())
                    .build());
        }
    }

    public static void setFloristMinimumData(MemberServiceResponse.MemberInfo memberInfo,
                                             SelectionFailureResponse.SelectionFailureInfo selectionFailureInfo,
                                             List<SelectionFailureResponse.MetaData> metaData) {
        BigDecimal floristMinimum = getMinimumValue(getFloristMinimum(memberInfo));
        if (floristMinimum.compareTo(selectionFailureInfo.getProductPrice()) > 0) {
            metaData.add(SelectionFailureResponse.MetaData.builder()
                    .reasonCode(ReasonCode.FS_FM)
                    .reasonData(SelectionFailureResponse.ReasonData.builder().floristMinimum(floristMinimum).build())
                    .build());
        }
    }

    public static void setProductMinimumData(MemberServiceResponse.CodificationInfo codificationInfo,
                                             SelectionFailureResponse.SelectionFailureInfo selectionFailureInfo,
                                             List<SelectionFailureResponse.MetaData> metaData) {
        BigDecimal productMinimum = getMinimumValue(getProductMinimum(codificationInfo, selectionFailureInfo));
        if (productMinimum.compareTo(selectionFailureInfo.getProductPrice()) > 0) {
            metaData.add(SelectionFailureResponse.MetaData.builder()
                    .reasonCode(ReasonCode.FS_CM)
                    .reasonData(SelectionFailureResponse.ReasonData.builder().productMinimum(productMinimum).build())
                    .build());
        }
    }


    /*
     * Multiple delivery dates.
     * */
    public static Flux<LocalDate> getDeliveryDates(SelectionFailureResponse.SelectionFailureInfo selectionFailureInfo) {
        List<LocalDate> deliveryDates = new ArrayList<>();
        deliveryDates.add(selectionFailureInfo.getDeliveryDate());
        if (Objects.nonNull(selectionFailureInfo.getDeliveryDateEnd())) {
            for (LocalDate deliveryDate = selectionFailureInfo.getDeliveryDate().plusDays(1)
                 ; (deliveryDate.isBefore(selectionFailureInfo.getDeliveryDateEnd()) || deliveryDate.isEqual(selectionFailureInfo.getDeliveryDateEnd()))
                    ; deliveryDate = deliveryDate.plusDays(1)) {
                deliveryDates.add(deliveryDate);
            }
        }
        return Flux.fromIterable(deliveryDates);
    }

    public static String getProductMinimum(MemberServiceResponse.CodificationInfo codificationInfo,
                                           SelectionFailureResponse.SelectionFailureInfo selectionFailureInfo) {
        String productMinimum = null;
        if (Objects.nonNull(selectionFailureInfo.getCodificationId())) {
            MemberServiceResponse.CodificationInfo.ProductGroup productGroup =
                    getProductGroup(codificationInfo, selectionFailureInfo.getCodificationId(), "WEBSITE");
            if (Objects.nonNull(productGroup)) {
                productMinimum = getPreferenceVal(productGroup.getPreferences(), "minimumPrice");
            }
        }
        if (Objects.isNull(productMinimum)) {
            productMinimum = "0";
        }
        return productMinimum;
    }

    public static String getFloristMinimum(MemberServiceResponse.MemberInfo memberInfo) {
        return getPreferenceVal(memberInfo.getPreferences(), "minimumOrderAmount");
    }

    public static String getPreferenceVal(List<MemberServiceResponse.Preference> preferences, String variable) {
        String minOrder = "0";
        if (CollectionUtils.isNotEmpty(preferences)) {
            Optional<MemberServiceResponse.Preference> optionalPreference = preferences.stream()
                    .filter(preference -> variable.equals(preference.getName())).findFirst();
            if (optionalPreference.isPresent()) {
                if (CollectionUtils.isNotEmpty(optionalPreference.get().getValue())) {
                    minOrder = optionalPreference.get().getValue().get(0);
                }
            }
        }
        return minOrder;
    }

    public static MemberServiceResponse.CodificationInfo.ProductGroup getProductGroup(MemberServiceResponse.CodificationInfo codificationInfo, String skuCode, String channel) {
        if (Objects.nonNull(codificationInfo) && CollectionUtils.isNotEmpty(codificationInfo.getProductGroups())) {
            Optional<MemberServiceResponse.CodificationInfo.ProductGroup> optionalProductGroup = codificationInfo.getProductGroups().stream()
                    .filter(productGroup -> productGroup.getChannel().equalsIgnoreCase(channel)
                            && productGroup.getCode().equalsIgnoreCase(skuCode))
                    .findFirst();
            if (optionalProductGroup.isPresent()) {
                return optionalProductGroup.get();
            }
        }
        return null;
    }

    public static Mono<SelectionFailureResponse.SelectionFailureInfo> setManualFlorists(SelectionFailureResponse.SelectionFailureInfo selectionFailureInfo) {
        HashMap<LocalDate, List<String>> availableFlorists = new HashMap<>();
        if (CollectionUtils.isNotEmpty(selectionFailureInfo.getFloristDetails())) {
            selectionFailureInfo.getFloristDetails().forEach(floristInfo -> {
                floristInfo.getDetails()
                        .forEach(details -> {
                            if (isAvailable(details)) {
                                includeMember(floristInfo.getMemberCode(), details.getDeliveryDate(), availableFlorists);
                            }
                        });
            });
        }
        selectionFailureInfo.getAvailableFlorists().stream()
                .forEach(ele -> {
                    ele.setManualFlorists(Optional.ofNullable(availableFlorists.get(ele.getDeliveryDate())).orElse(new ArrayList<>()));
                    Collections.sort(ele.getManualFlorists());
                });
        return Mono.just(selectionFailureInfo);
    }

    public static Boolean isAvailable(SelectionFailureResponse.Details details) {
        return details.getMetaData().stream().anyMatch(ele -> ele.getReasonCode().equals(ReasonCode.FS_AV));
    }


    public static void includeMember(String memberCode, LocalDate deliveryDate, HashMap<LocalDate, List<String>> availableFlorists) {
        if (availableFlorists.containsKey(deliveryDate)) {
            availableFlorists.get(deliveryDate).add(memberCode);
        } else {
            availableFlorists.put(deliveryDate, new ArrayList<>(Arrays.asList(memberCode)));
        }
    }


    public static BigDecimal getMinimumValue(String minimum) {
        if (Objects.nonNull(minimum) && !"null".equalsIgnoreCase(minimum) && !"NA".equalsIgnoreCase(minimum)) {
            return new BigDecimal(minimum);
        }
        return BigDecimal.valueOf(0);
    }

    /*
     * To filter florists if the product is non-codified.
     *   Remove florists who has florist minimum greater than product price.
     * */
    public static List<MemberServiceResponse> filterFlorists(List<MemberServiceResponse> members,
                                                             SelectionFailureResponse.SelectionFailureInfo selectionFailureInfo) {

        /*if (Objects.isNull(selectionFailureInfo.getCodificationId())) {
            members.removeIf(member -> getMinimumValue(getFloristMinimum(member.getMember().getMemberInfo()))
                    .compareTo(selectionFailureInfo.getProductPrice()) > 0);
        }*/
        return members;
    }

    public static SelectionFailureResponse.AvailableFloristsInfo setArgoFlorists(AvailabilityServiceResponse availabilityServiceResponse) {
        return SelectionFailureResponse.AvailableFloristsInfo.builder()
                .deliveryDate(availabilityServiceResponse.deliveryDate)
                .argoFlorists(availabilityServiceResponse.getFloristIds())
                .build();
    }
}
