package com.ftd.selection.failure.utility.service.service;

import com.ftd.selection.failure.utility.service.config.PmClosedConfig;
import com.ftd.selection.failure.utility.service.constants.CommonConstants;
import com.ftd.selection.failure.utility.service.domain.DirectoryServiceRequest;
import com.ftd.selection.failure.utility.service.domain.MemberServiceResponse;
import com.ftd.selection.failure.utility.service.domain.SelectionFailureResponse;
import com.ftd.selection.failure.utility.service.util.SelectionFailureUtil;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.core.scheduler.Schedulers;

import java.util.Collections;
import java.util.List;
import java.util.Objects;

@Service
public class SelectionFailureService {

    @Autowired
    private OrderService orderService;

    @Autowired
    private MemberService memberService;

    @Autowired
    private DirectoryService directoryService;

    @Autowired
    private AvailabilityService availabilityService;

    @Autowired
    private PmClosedConfig pmClosedConfig;

    public Mono<SelectionFailureResponse> getSelectionFailureResponse(String orderId, String deliveryGroupId, String siteId) {
        SelectionFailureResponse selectionFailureResponse = SelectionFailureResponse.builder().build();
        return this.setOrderDetails(orderId, deliveryGroupId, siteId, selectionFailureResponse)
                .flatMap(ele -> this.getMembersInformation(ele, selectionFailureResponse.getSiteId())
                        .flatMap(members ->
                                SelectionFailureUtil.analyzeBlocks(members, ele, pmClosedConfig))
                )
                .flatMap(ele -> setArgoFlorists(ele, selectionFailureResponse.getSiteId()))
                .flatMap(ele -> SelectionFailureUtil.setManualFlorists(ele))
                .collectList()
                .flatMap(list -> {
                    selectionFailureResponse.setSelectionFailureDetails(list);
                    return Mono.just(selectionFailureResponse);
                })
                // This method will execute when invalid orderId is given or when order service is down.
                // Remove this code and fall back method in order service if the message has to be retried again.
                .switchIfEmpty(Mono.just(SelectionFailureResponse.builder().build()));
    }


    public Mono<SelectionFailureResponse.SelectionFailureInfo> setArgoFlorists(SelectionFailureResponse.SelectionFailureInfo selectionFailureInfo,
                                                                               String siteId) {
        return availabilityService.getAvailableFlorists(siteId, selectionFailureInfo)
                .flatMapMany(Flux::fromIterable)
                .flatMap(ele -> Mono.just(SelectionFailureUtil.setArgoFlorists(ele)))
                .map(ele -> {
                    if (CollectionUtils.isNotEmpty(ele.getArgoFlorists())) {
                        Collections.sort(ele.getArgoFlorists());
                    }
                    return ele;
                })
                .collectList()
                .map(ele -> {
                    selectionFailureInfo.setAvailableFlorists(ele);
                    return selectionFailureInfo;
                });
    }

    public Flux<SelectionFailureResponse.SelectionFailureInfo> setOrderDetails(String orderId, String deliveryGroupId, String siteId,
                                                                               SelectionFailureResponse selectionFailureResponse) {
        return orderService.getOrderDetails(orderId,deliveryGroupId, siteId)
                .map(orderSearchResponse -> {
                    selectionFailureResponse.setSiteId(orderSearchResponse.getSiteId());
                    selectionFailureResponse.setOrderId(orderSearchResponse.getOrderId());
                    return orderSearchResponse;
                })
                .flatMapIterable(orderSearchResponse -> orderSearchResponse.getDeliveryGroups())
                .flatMap(deliveryGroup -> Mono.just(SelectionFailureUtil.setOrderDetails(deliveryGroup)));
    }

    public Mono<List<MemberServiceResponse>> getMembersInformation(SelectionFailureResponse.SelectionFailureInfo selectionFailureInfo,
                                                                   String siteId) {
        return directoryService.getMemberCodes(siteId, getDirectoryServiceRequest(selectionFailureInfo))
                .parallel()
                .runOn(Schedulers.parallel())
                .flatMap(memberCode ->
                        memberService.getMemberDetails(siteId, memberCode))
                .sequential()
                .collectList()
                .map(ele -> SelectionFailureUtil.filterFlorists(ele, selectionFailureInfo));
    }

    /*
     * Logic to include codificationId or not.
     *   if the product is codified and not of other category type then include as part of directory-service request.
     * */
    private DirectoryServiceRequest getDirectoryServiceRequest(SelectionFailureResponse.SelectionFailureInfo selectionFailureInfo) {
        DirectoryServiceRequest.DirectoryServiceRequestBuilder directoryServiceRequest =
                DirectoryServiceRequest.builder().deliveryZipcode(selectionFailureInfo.getZipCode());
        if (Objects.nonNull(selectionFailureInfo.getCodificationId())
                && !CommonConstants.OTHER.equalsIgnoreCase(selectionFailureInfo.getCodificationCategory())) {
            directoryServiceRequest.websiteProductGroup(selectionFailureInfo.getCodificationId());
        }
        return directoryServiceRequest.build();
    }
}
