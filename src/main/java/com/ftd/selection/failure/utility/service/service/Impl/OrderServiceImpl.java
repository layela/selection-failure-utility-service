package com.ftd.selection.failure.utility.service.service.Impl;

import com.ftd.selection.failure.utility.service.domain.OrderSearchResponse;
import com.ftd.selection.failure.utility.service.service.OrderService;
import com.ftd.selection.failure.utility.service.ws.client.OrderServiceRestApiClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Mono;

import java.util.ArrayList;

@Service
public class OrderServiceImpl implements OrderService {

    private static final Logger LOG = LoggerFactory.getLogger(OrderServiceImpl.class);


    @Autowired
    @Qualifier("sfu-orderServiceRestApiClient")
    private OrderServiceRestApiClient orderServiceRestApiClient;

    public Mono<OrderSearchResponse> getOrderDetails(String orderId, String deliveryGroupId, String siteId) {
        //Remove onErrorReturn method when you want to retry the message.
        //If not removed, the message will not result in error and will be processed with empty details.
        return orderServiceRestApiClient.getOderServiceResponse(orderId, deliveryGroupId, siteId)
                .onErrorReturn(throwable -> {
                    LOG.error("Method :: getOrderDetails : {}", throwable);
                    return true;
                }, getOrderDetailsFallbackResponse(orderId));
    }

    private OrderSearchResponse getOrderDetailsFallbackResponse(String orderId) {
        return OrderSearchResponse.builder().orderId(orderId).deliveryGroups(new ArrayList<>()).build();
    }
}
