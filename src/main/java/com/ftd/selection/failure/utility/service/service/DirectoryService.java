package com.ftd.selection.failure.utility.service.service;

import com.ftd.selection.failure.utility.service.domain.DirectoryServiceRequest;
import reactor.core.publisher.Flux;

public interface DirectoryService {

    Flux<String> getMemberCodes(String siteId, DirectoryServiceRequest directoryServiceRequest);
}
