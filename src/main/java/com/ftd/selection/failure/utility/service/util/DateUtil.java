package com.ftd.selection.failure.utility.service.util;

import org.apache.commons.lang3.StringUtils;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;

public final class DateUtil {

    public static LocalDateTime convertUtcToMemberTimeZone(String timeZone) {
        if (StringUtils.isEmpty(timeZone)) {
            timeZone = "UTC";
        }
        return ZonedDateTime.of(LocalDateTime.now(), ZoneOffset.UTC)
                .withZoneSameInstant(ZoneId.of(timeZone))
                .toLocalDateTime();
    }
}
