package com.ftd.selection.failure.utility.service.domain;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.List;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ProductServiceResponse implements Serializable {
    private static final long serialVersionUID = 1L;

    private List<Product> products;

    @Data
    @Builder
    @NoArgsConstructor
    @AllArgsConstructor
    public static class Product implements Serializable {

        private static final long serialVersionUID = 1L;
        private Classification classification;
    }

    @Data
    @Builder
    @NoArgsConstructor
    @AllArgsConstructor
    public static class Classification implements Serializable {

        private String type;
        private String codificationId;
        private String productClass;
        private Boolean isAddon;
    }
}
