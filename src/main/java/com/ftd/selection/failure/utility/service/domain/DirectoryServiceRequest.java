package com.ftd.selection.failure.utility.service.domain;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class DirectoryServiceRequest implements Serializable {

    private static final long serialVersionUID = 1L;
    private String websiteProductGroup;
    private String deliveryZipcode;
    @Builder.Default
    private String sendOnlyPreference = "ALL";
    @Builder.Default
    private String status = "Active";

}
