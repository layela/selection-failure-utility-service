package com.ftd.selection.failure.utility.service.domain;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.List;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class OrderSearchResponse implements Serializable {

    private static final long serialVersionUID = 1L;

    private String siteId;
    private String orderId;
    private List<DeliveryGroup> deliveryGroups;

    @Data
    @Builder
    @AllArgsConstructor
    @NoArgsConstructor
    public static class DeliveryGroup implements Serializable {
        private static final long serialVersionUID = 1L;

        private String deliveryGroupId;
        private LocalDate deliveryDate;
        private LocalDate deliveryDateEnd;
        private String orderStatus;

        private RecipientDetails recipientDetails;
        private List<LineItem> lineItems;
        private List<String> rejectedFulfillingMembers;

    }

    @Data
    @Builder
    @AllArgsConstructor
    @NoArgsConstructor
    public static class RecipientDetails implements Serializable {
        private static final long serialVersionUID = 1L;

        private String postalCode;
    }

    @Data
    @Builder
    @AllArgsConstructor
    @NoArgsConstructor
    public static class LineItem implements Serializable {
        private static final long serialVersionUID = 1L;

        private String productId;
        private String sku;
        private String codificationId;
        private LineItemAmounts lineItemAmounts;
    }

    @Data
    @Builder
    @AllArgsConstructor
    @NoArgsConstructor
    public static class LineItemAmounts implements Serializable {
        private static final long serialVersionUID = 1L;

        private Integer retailProductAmount;
    }
}
