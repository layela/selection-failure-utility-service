package com.ftd.selection.failure.utility.service.service;

import com.ftd.selection.failure.utility.service.domain.AvailabilityServiceResponse;
import com.ftd.selection.failure.utility.service.domain.SelectionFailureResponse;
import reactor.core.publisher.Mono;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;

public interface AvailabilityService {
    String BACKEND = "florist-availability-service";

    Mono<List<AvailabilityServiceResponse>> getAvailableFlorists(String siteId,
                                                                 SelectionFailureResponse.SelectionFailureInfo selectionFailureInfo);
}
