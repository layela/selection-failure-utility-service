package com.ftd.selection.failure.utility.service.config;

import com.fasterxml.jackson.databind.ObjectMapper;
import io.netty.channel.ChannelOption;
import io.netty.handler.codec.http.cookie.ClientCookieDecoder;
import io.netty.handler.codec.http.cookie.ClientCookieEncoder;
import io.netty.handler.ssl.SslContext;
import io.netty.handler.ssl.SslContextBuilder;
import io.netty.handler.ssl.util.InsecureTrustManagerFactory;
import io.netty.handler.timeout.ReadTimeoutHandler;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.client.reactive.ReactorClientHttpConnector;
import org.springframework.http.codec.json.Jackson2JsonDecoder;
import org.springframework.web.reactive.function.client.ExchangeFilterFunction;
import org.springframework.web.reactive.function.client.ExchangeStrategies;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;
import reactor.netty.http.client.HttpClient;
import reactor.netty.resources.ConnectionProvider;
import reactor.netty.tcp.TcpClient;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.TimeUnit;

@Configuration
@EnableConfigurationProperties({OrderServiceWSProperties.class, MemberServiceWSProperties.class,
        DirectoryServiceWSProperties.class, AvailabilityServiceWSProperties.class})
public class WebClientConfig {

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private OrderServiceWSProperties orderServiceWSProperties;

    @Autowired
    private DirectoryServiceWSProperties directoryServiceWSProperties;

    @Autowired
    private AvailabilityServiceWSProperties availabilityServiceWSProperties;

    @Autowired
    private MemberServiceWSProperties memberServiceWSProperties;
    private static final Logger LOGGER = LoggerFactory.getLogger(WebClientConfig.class);


    @Bean(name = "orderServiceWebClient")
    public WebClient orderServiceWebClient() {
        return getWebClient(orderServiceWSProperties, "order-service");
    }

    @Bean(name = "directoryServiceWebClient")
    public WebClient directoryServiceWebClient() {
        return getWebClient(directoryServiceWSProperties, "directory-service");
    }

    @Bean(name = "memberServiceWebClient")
    public WebClient memberServiceWebClient() {
        return getWebClient(memberServiceWSProperties, "member-service");
    }

    @Bean(name = "availabilityServiceWebClient")
    public WebClient availabilityServiceWebClient() {
        return getWebClient(availabilityServiceWSProperties, "availability-service");
    }

    private WebClient getWebClient(WSProperties wsProperties, String client) {
        return createWebClientBuilder(wsProperties, client).baseUrl(wsProperties.getUri()).build();
    }

    /*private ExchangeStrategies getExchangeStrategies() {
        return ExchangeStrategies.builder()
                .codecs(clientCodecConfigurer ->
                    clientCodecConfigurer.defaultCodecs()
                            .jackson2JsonDecoder(new Jackson2JsonDecoder(objectMapper, MediaType.APPLICATION_JSON))
                ).build();
    }*/

    //For higher version you need to use HttpClient from reactor.netty and configure the
    // connect and read timeout using tcpConfiguration.
  /*  private ReactorClientHttpConnector getReactorClientHttpConnector(
            SslContext sslContext, WSProperties wsProperties, String client) {
        return new ReactorClientHttpConnector(
                options ->
                        options.sslContext(sslContext)
                                .option(ChannelOption.CONNECT_TIMEOUT_MILLIS, getTimeOut(wsProperties))
                                .poolResources(PoolResources.fixed("order-utility-http-" + client,
                                        wsProperties.getPoolResource().getMaxConnections(),
                                        wsProperties.getPoolResource().getAcquireTimeout()))
                                .compression(true)
                                .afterNettyContextInit(ctx -> {
                                    ctx.addHandlerLast(new ReadTimeoutHandler(getTimeOut(wsProperties), TimeUnit.MILLISECONDS));
                                }));
    }*/

    private WebClient.Builder createWebClientBuilder(WSProperties wsProperties, String client) {
        WebClient.Builder result = WebClient.builder();
        try {
            SslContext sslContext = SslContextBuilder
                    .forClient()
                    .trustManager(InsecureTrustManagerFactory.INSTANCE)
                    .build();

            TcpClient tcpClient = TcpClient.create(ConnectionProvider.newConnection())
                    .secure(t -> t.sslContext(sslContext))
                    .option(ChannelOption.CONNECT_TIMEOUT_MILLIS, wsProperties.getTimeout())
                    .doOnConnected(connection ->
                            connection.addHandlerLast(new ReadTimeoutHandler(wsProperties.getTimeout(), TimeUnit.MILLISECONDS)));

            HttpClient httpClient = HttpClient.from(tcpClient).cookieCodec(ClientCookieEncoder.LAX,
                    ClientCookieDecoder.LAX);
            result = result.clientConnector(new ReactorClientHttpConnector(httpClient)).exchangeStrategies(getExchangeStrategies());
        } catch (Exception e) {
            LOGGER.warn("Issue while handling SSL certificate");
        }
        return result
                .defaultHeader(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                .defaultHeader(HttpHeaders.ACCEPT, MediaType.APPLICATION_JSON_VALUE)
                .defaultHeader("clientId", client);
    }

    private SslContext getSslContext() {
        SslContext sslContext = null;
        try {
            sslContext = SslContextBuilder.forClient().trustManager(InsecureTrustManagerFactory.INSTANCE).build();
        } catch (Exception e) {
            LOGGER.warn("{}", "exception occurred while creating ssl-context", e);
        }
        return sslContext;
    }

    private Integer getTimeOut(WSProperties wsProperties) {
        RetryableProperties retryableProperties = wsProperties.getRetryableProperties();
        if (Objects.nonNull(retryableProperties) && retryableProperties.getNumRetries() > 0) {
            return retryableProperties.getTimeout();
        }
        return wsProperties.getTimeout();
    }

    private ExchangeFilterFunction logRequest() {
        return ExchangeFilterFunction.ofRequestProcessor(clientRequest -> {
            List<String> headersList = new ArrayList<>();
            clientRequest.headers()
                    .forEach((name, values) -> values.forEach(value ->
                            headersList.add(new StringBuilder(name).append("=").append(value).toString())));
            LOGGER.info("#Request: #HttpMethod: {} #Url: {} #Headers: {}", clientRequest.method(), clientRequest.url(), headersList.toString());
            return Mono.just(clientRequest);
        });
    }

    private ExchangeStrategies getExchangeStrategies() {
        return ExchangeStrategies.builder()
                .codecs(clientCodecConfigurer -> {
                    clientCodecConfigurer.defaultCodecs().maxInMemorySize(16 * 1024 * 1024);
                }).build();
    }
}

@Data
@EqualsAndHashCode(callSuper = true)
@ConfigurationProperties(prefix = "properties.order.service")
class OrderServiceWSProperties extends WSProperties {

}

@Data
@EqualsAndHashCode(callSuper = true)
@ConfigurationProperties(prefix = "properties.member.service")
class MemberServiceWSProperties extends WSProperties {

}

@Data
@EqualsAndHashCode(callSuper = true)
@ConfigurationProperties(prefix = "properties.directory.service")
class DirectoryServiceWSProperties extends WSProperties {

}

@Data
@EqualsAndHashCode(callSuper = true)
@ConfigurationProperties(prefix = "properties.availability.service")
class AvailabilityServiceWSProperties extends WSProperties {

}

@Data
class WSProperties {
    private String uri;
    private Integer timeout;
    private PoolResource poolResource;
    private RetryableProperties retryableProperties;
}

@Data
class RetryableProperties {
    private Integer numRetries;
    private Integer timeout;
}

@Data
class PoolResource {
    private Integer maxConnections;
    private Integer acquireTimeout;
}
