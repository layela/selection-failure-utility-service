package com.ftd.selection.failure.utility.service.domain;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.List;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class MemberServiceResponse implements Serializable {
    private static final long serialVersionUID = 1L;

    private Identity identity;
    private Member member;

    @Data
    @Builder
    @NoArgsConstructor
    @AllArgsConstructor
    public static class Identity implements Serializable {
        private static final long serialVersionUID = 1L;
        private String memberCode;
        private String status;
    }
    @Data
    @Builder
    @NoArgsConstructor
    @AllArgsConstructor
    public static class Member implements Serializable {
        private static final long serialVersionUID = 1L;

        private Identity identity;
        private String source;
        private String status;
        private String legacyMemberId;
        private MemberInfo memberInfo;
        private CodificationInfo codificationInfo;
        private OperationalHoursInfo operationalHoursInfo;
        private DeliveryZoneInfo deliveryZoneInfo;
        private AvailabilityExceptionInfo availabilityExceptionInfo;
    }
    @Data
    @Builder
    @NoArgsConstructor
    @AllArgsConstructor
    public static class MemberInfo implements Serializable {
        private static final long serialVersionUID = 1L;

        private List<Preference> preferences;

        private GeoInfo geoInfo;

    }

    @Data
    @Builder
    @NoArgsConstructor
    @AllArgsConstructor
    public static class GeoInfo implements Serializable {
        private static final long serialVersionUID = 1L;

        private String division;
        private String territory;
        private String longitude;
        private String latitude;
        private String timeZone;
        private Boolean isDSTObserved;
        private String timeOffsetUTC;
        private String tzData;
    }

    @Data
    @Builder
    @NoArgsConstructor
    @AllArgsConstructor
    public static class Preference implements Serializable {
        private static final long serialVersionUID = 1L;

        private String name;
        private List<String> value;
    }

    @Data
    @Builder
    @NoArgsConstructor
    @AllArgsConstructor
    public static class CodificationInfo implements Serializable {
        private static final long serialVersionUID = 1L;

        private List<ProductGroup> productGroups;

        @Data
        @Builder
        @NoArgsConstructor
        @AllArgsConstructor
        public static class ProductGroup implements Serializable {
            private static final long serialVersionUID = 1L;

            private String id;
            private String description;
            private String category;
            private String code;
            private String channel;
            private List<Preference> preferences;
        }
    }

    @Data
    @Builder
    @NoArgsConstructor
    @AllArgsConstructor
    public static class OperationalHoursInfo implements Serializable {

        private static final long serialVersionUID = 1L;

        private List<OperationalHour> operationalHours;

        @Data
        @Builder
        @NoArgsConstructor
        @AllArgsConstructor
        public static class OperationalHour implements Serializable {

            private static final long serialVersionUID = 1L;
            private String channel;
            private List<WorkingHours> openHours;
            private List<WorkingHourOverride> openHoursOverrides;
        }

        @Data
        @Builder
        @NoArgsConstructor
        @AllArgsConstructor
        public static class WorkingHours implements Serializable {
            private static final long serialVersionUID = 1L;

            private String day;
            private String code;
            private String openTime;
            private String closeTime;
            private String openedFlag;
        }

        @Data
        @Builder
        @NoArgsConstructor
        @AllArgsConstructor
        public static class WorkingHourOverride implements Serializable {
            private static final long serialVersionUID = 1L;

            private String id;
            private LocalDate startDate;
            private LocalDate endDate;
        }
    }
}
