package com.ftd.selection.failure.utility.service.domain;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.ftd.selection.failure.utility.service.constants.DeliveryOption;
import com.ftd.selection.failure.utility.service.constants.DeliveryZone;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.List;

/**
 * @author layela
 *
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class DeliveryZoneInfo implements Serializable {
	private static final long serialVersionUID = 1L;

	private List<DeliveryZoneDefaults> defaults;

	private List<DeliveryZoneGen> deliveryZones;

	@Data
	@Builder
	@NoArgsConstructor
	@AllArgsConstructor
	public static class DeliveryZoneDefaults implements Serializable {
		private static final long serialVersionUID = 1L;

		@JsonProperty
		private String channel;
		private List<DeliveryOptionDefaults> deliveryOptions;
	}

	@Data
	@Builder
	@NoArgsConstructor
	@AllArgsConstructor
	public static class DeliveryOptionDefaults implements Serializable {
		private static final long serialVersionUID = 1L;

		private DeliveryOption deliveryOption;
		private CutoffTime cutoffTime;
	}

	@Data
	@Builder
	@NoArgsConstructor
	@AllArgsConstructor
	public static class DeliveryZoneGen implements Serializable {
		private static final long serialVersionUID = 1L;

		private String id;
		private String name; // holds name of delivery-zone
		private DeliveryZone type; // holds type of delivery-zone
		private boolean enabled;
		@JsonProperty
		private String channel;
		private ZoneTypeInfo zoneTypeInfo;
		private List<ProductGroupInfo> productGroups;

	}

	@Data
	@Builder
	@NoArgsConstructor
	@AllArgsConstructor
	public static class ZoneTypeInfo implements Serializable {
		private static final long serialVersionUID = 1L;

		private ZipCodeInfo zipCodeInfo;
	}

	@Data
	@Builder
	@NoArgsConstructor
	@AllArgsConstructor
	public static class ZipCodeInfo implements Serializable {
		private static final long serialVersionUID = 1L;

		private String zipcode;
		private List<String> cities;
		private String state;
	}

	@Data
	@Builder
	@NoArgsConstructor
	@AllArgsConstructor
	public static class ProductGroupInfo implements Serializable {
		private static final long serialVersionUID = 1L;

		private List<String> value;

		private List<DeliveryOptionInfo> deliveryOptions;
	}

	@Data
	@Builder
	@NoArgsConstructor
	@AllArgsConstructor
	public static class DeliveryOptionInfo implements Serializable {
		private static final long serialVersionUID = 1L;
		private DeliveryOption deliveryOption;
		private CutoffTime cutoffTime;
	}

	@Data
	@Builder
	@NoArgsConstructor
	@AllArgsConstructor
	public static class CutoffTime implements Serializable {
		private static final long serialVersionUID = 1L;
		private WeekDayCutoff local;
	}

	@Data
	@Builder
	@NoArgsConstructor
	@AllArgsConstructor
	public static class WeekDayCutoff implements Serializable {
		private static final long serialVersionUID = 1L;
		private String mon;
		private String tue;
		private String wed;
		private String thu;
		private String fri;
		private String sat;
		private String sun;
	}
}
