package com.ftd.selection.failure.utility.service.config;

import lombok.Data;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

@Data
@Configuration
public class PmClosedConfig {

    @Value("${properties.pm-closed.delivery-cutoff}")
    private String pmClosedDeliveryCutOff;

    @Value("${properties.pm-closed.order-cutoff}")
    private String pmClosedOrderCutOff;
}
