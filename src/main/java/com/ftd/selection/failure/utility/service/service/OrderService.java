package com.ftd.selection.failure.utility.service.service;

import com.ftd.selection.failure.utility.service.domain.OrderSearchResponse;
import reactor.core.publisher.Mono;

public interface OrderService {

    String BACKEND = "order-service";

    Mono<OrderSearchResponse> getOrderDetails(String orderId, String deliveryGroupId, String siteId);
}
