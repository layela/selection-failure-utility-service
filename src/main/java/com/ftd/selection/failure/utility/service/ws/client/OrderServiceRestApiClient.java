package com.ftd.selection.failure.utility.service.ws.client;

import com.ftd.selection.failure.utility.service.domain.OrderSearchResponse;
import com.ftd.selection.failure.utility.service.util.WSClientUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;
import reactor.core.publisher.SignalType;

import java.util.logging.Level;

@Component("sfu-orderServiceRestApiClient")
public class OrderServiceRestApiClient {

    private static final Logger LOG = LoggerFactory.getLogger(OrderServiceRestApiClient.class);

    private static final String URI_ORDER_SERVICE = "/{siteId}/orders/{orderId}/deliveryGroups/{deliveryGroupId}/full";

    @Value("${properties.order.service.retryable-properties.numRetries}")
    private Integer numRetries;

    @Autowired
    @Qualifier(value = "orderServiceWebClient")
    private WebClient webClient;


    public Mono<OrderSearchResponse> getOderServiceResponse(String orderId, String deliveryGroupId, String siteId) {
        return webClient.get()
                .uri(uriBuilder -> WSClientUtil.getOrderServiceURI(uriBuilder, URI_ORDER_SERVICE, siteId,orderId, deliveryGroupId))
                .retrieve()
                .bodyToMono(OrderSearchResponse.class)
                .onErrorMap(WSClientUtil::getWSExceptionPredicate,
                        throwable -> WSClientUtil.throwWSException(throwable, "getOrderDetails", orderId))
                .retry(numRetries,
                        throwable -> WSClientUtil.retryPredicate(throwable, numRetries))
                .onErrorMap(WSClientUtil::getWSClientExceptionPredicate,
                        throwable -> WSClientUtil.throwWSClientException(throwable, "getOrderDetails", orderId))
                .log("order-service.reactor.",
                        Level.INFO,
                        SignalType.CANCEL,
                        SignalType.ON_ERROR,
                        SignalType.ON_COMPLETE);
    }

}
