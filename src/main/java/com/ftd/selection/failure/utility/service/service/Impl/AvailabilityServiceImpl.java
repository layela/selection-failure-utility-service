package com.ftd.selection.failure.utility.service.service.Impl;

import com.ftd.selection.failure.utility.service.domain.AvailabilityServiceResponse;
import com.ftd.selection.failure.utility.service.domain.SelectionFailureResponse;
import com.ftd.selection.failure.utility.service.service.AvailabilityService;
import com.ftd.selection.failure.utility.service.ws.client.AvailabilityServiceRestApiClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Mono;

import java.util.List;

@Service
public class AvailabilityServiceImpl implements AvailabilityService {


    @Autowired
    private AvailabilityServiceRestApiClient availabilityServiceRestApiClient;

    public Mono<List<AvailabilityServiceResponse>> getAvailableFlorists(String siteId,
                                                                        SelectionFailureResponse.SelectionFailureInfo selectionFailureInfo) {
        return availabilityServiceRestApiClient.getAvailabilityServiceResponse(siteId, selectionFailureInfo.getProductId(), selectionFailureInfo.getProductPrice(),
                selectionFailureInfo.getZipCode(), selectionFailureInfo.getDeliveryDate(), selectionFailureInfo.getDeliveryDateEnd());
    }
}
