package com.ftd.selection.failure.utility.service.domain;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class AvailabilityExceptionInfo implements Serializable {

    private List<CityInfo> cityBlocks;
    private List<ZipInfo> zipBlocks;
    private List<FloristBlockInfo> floristBlocks;
    private List<CodificationInfo> codificationBlocks;
    private List<SuspendInfo> suspendBlocks;

    @Data
    @NoArgsConstructor
    @AllArgsConstructor
    public static class DateRange {
        @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'")
        private LocalDateTime startDate;
        @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'")
        private LocalDateTime endDate;
    }

    @Data
    @Builder
    @NoArgsConstructor
    @AllArgsConstructor
    public static class CityInfo extends DateRange implements Serializable {
        private static final long serialVersionUID = 1L;
        private String exceptionId;
        private String cityName;
        private String listingCode;
        private String type;

    }

    @Data
    @Builder
    @NoArgsConstructor
    @AllArgsConstructor
    public static class FloristBlockInfo extends DateRange implements Serializable {
        private static final long serialVersionUID = 1L;

        private String exceptionId;
        private String type;
        private String blockedBy;
    }

    @Data
    @Builder
    @NoArgsConstructor
    @AllArgsConstructor
    public static class ZipInfo extends DateRange implements Serializable {
        private static final long serialVersionUID = 1L;

        private String exceptionId;
        private String zipCode;
    }

    @Data
    @Builder
    @NoArgsConstructor
    @AllArgsConstructor
    public static class SuspendInfo extends DateRange implements Serializable {
        private static final long serialVersionUID = 1L;

        private String exceptionId;
        private String type;
        @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'")
        private Date gotoFloristSuspendReceived;
        @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'")
        private Date gotoFloristResumeReceived;
    }

    @Data
    @Builder
    @NoArgsConstructor
    @AllArgsConstructor
    public static class CodificationInfo extends DateRange implements Serializable {
        private static final long serialVersionUID = 1L;

        private String exceptionId;
        private String productGroup;
    }

}
