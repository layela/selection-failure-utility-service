package com.ftd.selection.failure.utility.service.service.Impl;

import com.ftd.selection.failure.utility.service.domain.DirectoryServiceRequest;
import com.ftd.selection.failure.utility.service.service.DirectoryService;
import com.ftd.selection.failure.utility.service.ws.client.DirectoryServiceRestApiClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;

import java.util.stream.Collectors;

@Service
public class DirectoryServiceImpl implements DirectoryService {

    @Autowired
    private DirectoryServiceRestApiClient directoryServiceRestApiClient;

    public Flux<String> getMemberCodes(String siteId, DirectoryServiceRequest directoryServiceRequest) {
        return directoryServiceRestApiClient.getDirectoryServiceResponse(siteId, directoryServiceRequest)
                .flatMapIterable(ele ->
                        ele.getContent()
                                .stream().map(member -> member.getId()).collect(Collectors.toList()));
    }

}
