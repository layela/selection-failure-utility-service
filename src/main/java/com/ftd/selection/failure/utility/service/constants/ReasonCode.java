package com.ftd.selection.failure.utility.service.constants;

public enum ReasonCode {
    FS_ZB,
    FS_CIB,
    FS_FB,
    FS_SB,
    FS_FM,
    FS_CM,
    FS_SC,
    FS_CO,
    FS_AV
}
