package com.ftd.selection.failure.utility.service.ws.client;

import com.ftd.selection.failure.utility.service.domain.MemberServiceResponse;
import com.ftd.selection.failure.utility.service.util.WSClientUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;
import reactor.core.publisher.SignalType;

import java.util.logging.Level;

@Component
public class MemberServiceRestApiClient {

    @Autowired
    @Qualifier(value = "memberServiceWebClient")
    private WebClient webClient;

    @Value("${properties.member.service.retryable-properties.numRetries}")
    private Integer numRetries;

    private static final String URI_MEMBER_SERVICE = "/{siteId}/members/{memberCode}/getMember";

    public Mono<MemberServiceResponse> getMemberServiceResponse(String siteId, String memberCode) {
        return webClient.get()
                .uri(uriBuilder -> WSClientUtil.
                        getMemberServiceURI(uriBuilder, URI_MEMBER_SERVICE, siteId, memberCode))
                .retrieve()
                .bodyToMono(MemberServiceResponse.class)
                .onErrorMap(WSClientUtil::getWSExceptionPredicate,
                        throwable -> WSClientUtil.throwWSException(throwable, "getMemberDetails", memberCode))
                .retry(numRetries,
                        throwable -> WSClientUtil.retryPredicate(throwable, numRetries))
                .onErrorMap(WSClientUtil::getWSClientExceptionPredicate,
                        throwable -> WSClientUtil.throwWSClientException(throwable, "getMemberDetails", memberCode))
                .log("member-service.reactor.",
                        Level.INFO,
                        SignalType.CANCEL,
                        SignalType.ON_ERROR,
                        SignalType.ON_COMPLETE);
    }


}
