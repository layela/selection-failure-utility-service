package com.ftd.selection.failure.utility.service.service;

import com.ftd.selection.failure.utility.service.domain.MemberServiceResponse;
import reactor.core.publisher.Mono;

public interface MemberService {

    Mono<MemberServiceResponse> getMemberDetails(String siteId, String memberCode);
}
