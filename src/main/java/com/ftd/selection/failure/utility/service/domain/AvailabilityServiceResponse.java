package com.ftd.selection.failure.utility.service.domain;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.List;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class AvailabilityServiceResponse implements Serializable {

    private static final long serialVersionUID = 1L;

    public LocalDate deliveryDate;
    @JsonProperty(value = "floristId")
    public List<String> floristIds;
}
